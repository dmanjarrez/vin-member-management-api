﻿using Ninject.Modules;
using Vin.MemberManagement.Application;
using Vin.MemberManagement.Application.Contracts;

namespace Ninject.Extensions.Application
{
    public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMemberService>().To<MemberService>();
        }
    }
}
