﻿using Ninject.Modules;
using Ninject.Web.Common;
using System.Configuration;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.EntityFramework;
using Vin.MemberManagement.Infrastructure;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Contracts.Validations;
using Vin.MemberManagement.Infrastructure.Sorting;
using Vin.MemberManagement.Infrastructure.Specifications;
using Vin.MemberManagement.Infrastructure.Validations;
using Vin.MemberManagement.InfrastructureRepositories;

namespace Ninject.Extensions.Persistence
{
    public class InfrastructureModule : NinjectModule
    {
        private const string ConnectionStringKeyKey = "EntityContext";
        private const string defaultPageSizeKey = "defaultPageSize";

        public override void Load()
        {
            Bind<IUnitOfWork>().To<EntityFrameworkUnitOfWork>();
            Bind<IDatabaseContext>().To<MemberManagementDbContext>()
                .InRequestScope()
                .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings[ConnectionStringKeyKey].ToString());

            Bind<IMemberRepository>().To<MemberRepository>();

            Bind<IMemberSpecificationFactory>().To<MemberSpecificationFactory>();

            Bind<IValidationResultBuilder>().To<ValidationResultBuilder>();
            Bind<IMemberValidations>().To<MemberValidations>();
            Bind<IValidator<Member>>().To<MemberValidator>();
            Bind<ISortingService>().To<SortingService>();
            Bind<IPagingService>()
                .To<PagingService>()
                .WithConstructorArgument("defaultPageSize", int.Parse(ConfigurationManager.AppSettings[defaultPageSizeKey].ToString()));
            Bind<ISpecificationCombinerService>().To<SpecificationCombinerService>();
        }
    }
}
