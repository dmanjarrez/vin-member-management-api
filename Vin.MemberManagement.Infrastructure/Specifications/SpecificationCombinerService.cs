﻿using System.Collections.Generic;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Infrastructure.Specifications
{
    public class SpecificationCombinerService : ISpecificationCombinerService
    {
        public ISpecification<TEntity> Union<TEntity>(IList<ISpecification<TEntity>> specifications)
        {
            ISpecification<TEntity> unionSpec = new ExpressionSpecification<TEntity>(te => true);

            if (specifications.Count == 0)
                return unionSpec;

            foreach(var spec in specifications)
            {
                var curBaseSpec = unionSpec;
                unionSpec = unionSpec.And(spec);
            }

            return unionSpec;
        }

        public ISpecification<TEntity> Intersection<TEntity>(IList<ISpecification<TEntity>> specifications)
        {
            ISpecification<TEntity> unionSpec = new ExpressionSpecification<TEntity>(te => true);

            if (specifications.Count == 0)
                return unionSpec;

            foreach (var spec in specifications)
            {
                var curBaseSpec = unionSpec;
                unionSpec = unionSpec.Or(spec);
            }

            return unionSpec;
        }
    }
}
