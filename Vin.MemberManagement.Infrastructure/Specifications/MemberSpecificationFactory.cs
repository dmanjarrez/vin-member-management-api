﻿using System;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Infrastructure.Specifications
{
    public class MemberSpecificationFactory : IMemberSpecificationFactory
    {
        #region Helpers
        private bool lt(string str1, string str2)
        {
            return string.Compare(str1, str2) < 0;
        }

        private bool lteq(string str1, string str2)
        {
            return string.Compare(str1, str2) < 0 || str1 == str2;
        }

        private bool gt(string str1, string str2)
        {
            return string.Compare(str1, str2) > 0;
        }

        private bool gteq(string str1, string str2)
        {
            return string.Compare(str1, str2) > 0 || str1 == str2;
        }
        #endregion

        public ISpecification<Member> IdEquals(Guid id)
        {
            return new ExpressionSpecification<Member>(m => m.Id == id);
        }

        #region Username
        public ISpecification<Member> UsernameValueLessThan(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => string.Compare(m.Username.Value, usernameValue) < 0);
        }

        public ISpecification<Member> UsernameValueLessThanOrEqualTo(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => lteq(m.Username.Value, usernameValue));
        }

        public ISpecification<Member> UsernameValueEquals(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => m.Username.Value == usernameValue);
        }

        public ISpecification<Member> UsernameValueGreaterThan(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => gt(m.Username.Value, usernameValue));
        }

        public ISpecification<Member> UsernameValueGreaterThanOrEqualTo(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => gteq(m.Username.Value, usernameValue));
        }
        #endregion

        #region Lastname
        public ISpecification<Member> LastnameValueLessThan(string lastname)
        {
            return new ExpressionSpecification<Member>(m => string.Compare(m.Name.Last, lastname) < 0);
        }

        public ISpecification<Member> LastnameValueLessThanOrEqualTo(string lastname)
        {
            return new ExpressionSpecification<Member>(m => lteq(m.Name.Last, lastname));
        }

        public ISpecification<Member> LastnameValueEquals(string lastname)
        {
            return new ExpressionSpecification<Member>(m => m.Name.Last == lastname);
        }

        public ISpecification<Member> LastnameValueGreaterThan(string lastname)
        {
            return new ExpressionSpecification<Member>(m => gt(m.Name.Last, lastname));
        }

        public ISpecification<Member> LastnameValueGreaterThanOrEqualTo(string lastname)
        {
            return new ExpressionSpecification<Member>(m => gteq(m.Name.Last, lastname));
        }
        #endregion

        #region Firstname
        public ISpecification<Member> FirstnameValueLessThan(string firstname)
        {
            return new ExpressionSpecification<Member>(m => string.Compare(m.Name.First, firstname) < 0);
        }

        public ISpecification<Member> FirstnameValueLessThanOrEqualTo(string firstname)
        {
            return new ExpressionSpecification<Member>(m => lteq(m.Name.First, firstname));
        }

        public ISpecification<Member> FirstnameValueEquals(string firstname)
        {
            return new ExpressionSpecification<Member>(m => m.Name.First == firstname);
        }

        public ISpecification<Member> FirstnameValueGreaterThan(string firstname)
        {
            return new ExpressionSpecification<Member>(m => gt(m.Name.First, firstname));
        }

        public ISpecification<Member> FirstnameValueGreaterThanOrEqualTo(string firstname)
        {
            return new ExpressionSpecification<Member>(m => gteq(m.Name.First, firstname));
        }
        #endregion

        #region DateOfBirth
        public ISpecification<Member> DateOfBirthLessThan(DateTime dateOfBirth)
        {
            return new ExpressionSpecification<Member>(m => m.DateOfBirth < dateOfBirth);
        }

        public ISpecification<Member> DateOfBirthLessThanOrEqualTo(DateTime dateOfBirth)
        {
            return new ExpressionSpecification<Member>(m => m.DateOfBirth <= dateOfBirth);
        }

        public ISpecification<Member> DateOfBirthEquals(DateTime dateOfBirth)
        {
            return new ExpressionSpecification<Member>(m => m.DateOfBirth == dateOfBirth);
        }

        public ISpecification<Member> DateOfBirthGreaterThan(DateTime dateOfBirth)
        {
            return new ExpressionSpecification<Member>(m => m.DateOfBirth > dateOfBirth);
        }

        public ISpecification<Member> DateOfBirthGreaterThanOrEqualTo(DateTime dateOfBirth)
        {
            return new ExpressionSpecification<Member>(m => m.DateOfBirth >= dateOfBirth);
        }
        #endregion

        #region EmailAddress
        public ISpecification<Member> EmailAddressEquals(string emailAddress)
        {
            return new ExpressionSpecification<Member>(m => m.EmailAddress.Value == emailAddress);
        }
        #endregion

        #region PhoneNumber
        public ISpecification<Member> PhoneNumberEquals(string phoneNumber)
        {
            return new ExpressionSpecification<Member>(m => m.PhoneNumber.Value == phoneNumber);
        }
        #endregion

        #region CreatedDate
        public ISpecification<Member> CreatedDateLessThan(DateTime createdDate)
        {
            return new ExpressionSpecification<Member>(m => m.CreatedDate < createdDate);
        }

        public ISpecification<Member> CreatedDateLessThanOrEqualTo(DateTime createdDate)
        {
            return new ExpressionSpecification<Member>(m => m.CreatedDate <= createdDate);
        }

        public ISpecification<Member> CreatedDateEquals(DateTime createdDate)
        {
            return new ExpressionSpecification<Member>(m => m.CreatedDate == createdDate);
        }

        public ISpecification<Member> CreatedDateGreaterThan(DateTime createdDate)
        {
            return new ExpressionSpecification<Member>(m => m.CreatedDate > createdDate);
        }

        public ISpecification<Member> CreatedDateGreaterThanOrEqualTo(DateTime createdDate)
        {
            return new ExpressionSpecification<Member>(m => m.CreatedDate >= createdDate);
        }
        #endregion

        #region LastUpdatedDate
        public ISpecification<Member> LastUpdatedDateLessThan(DateTime lastUpdatedDate)
        {
            return new ExpressionSpecification<Member>(m => m.LastUpdatedDate < lastUpdatedDate);
        }

        public ISpecification<Member> LastUpdatedDateLessThanOrEqualTo(DateTime lastUpdatedDate)
        {
            return new ExpressionSpecification<Member>(m => m.LastUpdatedDate <= lastUpdatedDate);
        }

        public ISpecification<Member> LastUpdatedDateEquals(DateTime lastUpdatedDate)
        {
            return new ExpressionSpecification<Member>(m => m.LastUpdatedDate == lastUpdatedDate);
        }

        public ISpecification<Member> LastUpdatedDateGreaterThan(DateTime lastUpdatedDate)
        {
            return new ExpressionSpecification<Member>(m => m.LastUpdatedDate > lastUpdatedDate);
        }

        public ISpecification<Member> LastUpdatedDateGreaterThanOrEqualTo(DateTime lastUpdatedDate)
        {
            return new ExpressionSpecification<Member>(m => m.LastUpdatedDate >= lastUpdatedDate);
        }

        public ISpecification<Member> UsernameLike(string usernameValue)
        {
            return new ExpressionSpecification<Member>(m => m.Username.Value.Contains(usernameValue));
        }

        public ISpecification<Member> LastNameLike(string lastname)
        {
            return new ExpressionSpecification<Member>(m => m.Name.Last.Contains(lastname));
        }

        public ISpecification<Member> FirstNameLike(string firstname)
        {
            return new ExpressionSpecification<Member>(m => m.Name.First.Contains(firstname));
        }

        public ISpecification<Member> EmailAddressLike(string emailAddress)
        {
            return new ExpressionSpecification<Member>(m => m.EmailAddress.Value.Contains(emailAddress));
        }

        public ISpecification<Member> PhoneNumberLike(string phoneNumber)
        {
            return new ExpressionSpecification<Member>(m => m.PhoneNumber.Value.Contains(phoneNumber));
        }
        #endregion
    }
}
