﻿using System;
using System.Linq.Expressions;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Infrastructure.Specifications
{
    public class ExpressionSpecification<TEntity> : ISpecification<TEntity>
    {
        private class ParameterReplacer : ExpressionVisitor
        {
            private readonly ParameterExpression paramExp;

            public ParameterReplacer(ParameterExpression paramExp)
            {
                this.paramExp = paramExp;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return base.VisitParameter(paramExp);
            }
        }

        public Expression<Func<TEntity, bool>> FuncExpression { get; }

        public ExpressionSpecification(Func<TEntity, bool> predicate)
        {
            FuncExpression = (something) => predicate(something);
        }

        public ISpecification<TEntity> And(ISpecification<TEntity> otherSpec)
        {
            var exp1 = FuncExpression;
            var exp2 = otherSpec.FuncExpression;

            var paramExp = Expression.Parameter(typeof(TEntity));
            var bodyExp = Expression.AndAlso(exp1.Body, exp2.Body);
            bodyExp = (BinaryExpression)new ParameterReplacer(paramExp).Visit(bodyExp);

            return new ExpressionSpecification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(bodyExp, paramExp).Compile());
        }

        public ISpecification<TEntity> AndNot(ISpecification<TEntity> otherSpec)
        {
            var exp1 = FuncExpression;
            var exp2 = otherSpec.FuncExpression;

            var paramExp = Expression.Parameter(typeof(TEntity));
            var bodyExp = Expression.AndAlso(exp1.Body, Expression.Not(exp2.Body));
            bodyExp = (BinaryExpression)new ParameterReplacer(paramExp).Visit(bodyExp);

            return new ExpressionSpecification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(bodyExp, paramExp).Compile());
        }

        public ISpecification<TEntity> Or(ISpecification<TEntity> otherSpec)
        {
            var exp1 = FuncExpression;
            var exp2 = otherSpec.FuncExpression;

            var paramExp = Expression.Parameter(typeof(TEntity));
            var bodyExp = Expression.OrElse(exp1.Body, exp2.Body);
            bodyExp = (BinaryExpression)new ParameterReplacer(paramExp).Visit(bodyExp);

            return new ExpressionSpecification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(bodyExp, paramExp).Compile());
        }

        public ISpecification<TEntity> OrNot(ISpecification<TEntity> otherSpec)
        {
            var exp1 = FuncExpression;
            var exp2 = otherSpec.FuncExpression;

            var paramExp = Expression.Parameter(typeof(TEntity));
            var bodyExp = Expression.OrElse(exp1.Body, Expression.Not(exp2.Body));
            bodyExp = (BinaryExpression)new ParameterReplacer(paramExp).Visit(bodyExp);

            return new ExpressionSpecification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(bodyExp, paramExp).Compile());
        }

        public ISpecification<TEntity> Not()
        {
            var exp = FuncExpression;

            var paramExp = Expression.Parameter(typeof(TEntity));
            var bodyExp = Expression.Not(exp.Body);
            bodyExp = (UnaryExpression)new ParameterReplacer(paramExp).Visit(bodyExp);

            return new ExpressionSpecification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(bodyExp, paramExp).Compile());
        }

        public bool IsSatisifedBy(TEntity candidate)
        {
            return FuncExpression.Compile().Invoke(candidate);
        }
    }
}