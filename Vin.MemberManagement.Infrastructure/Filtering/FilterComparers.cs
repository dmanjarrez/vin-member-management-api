﻿public enum FilterComparers
{
    eq = 1,
    neq = 2,
    lt = 3,
    lteq = 4,
    gt = 5,
    gteq = 6,
    like = 7
}