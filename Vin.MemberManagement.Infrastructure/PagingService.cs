﻿using System.Linq;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Infrastructure
{
    public class PagingService : IPagingService
    {
        public PagingService(int defaultPageSize)
        {
            DefaultPageSize = defaultPageSize;
        }

        public int DefaultPageSize { get; }

        public IQueryable<TEntity> Page<TEntity>(IQueryable<TEntity> items, int totalRecords, int pageNumber, int pageSize)
        {
            var actualPageNumber = GetActualPageNumber(totalRecords, pageNumber, pageSize);
            var actualPageSize = GetActualPageSize(totalRecords, pageNumber, pageSize);
            var skippedRecords = GetSkippedRecords(totalRecords, pageNumber, pageSize);

            return items
                .Skip(skippedRecords)
                .Take(actualPageSize);
        }

        public int GetActualPageNumber(int totalRecords, int requestedPageNumber, int requestedPageSize)
        {
            if (requestedPageNumber < 1)
                return 1;
            else if (requestedPageSize >= totalRecords)
                return 1;
            else if (requestedPageNumber * requestedPageSize > totalRecords)
                return totalRecords % requestedPageSize == 0
                        ? totalRecords / requestedPageSize
                        : totalRecords / requestedPageSize + 1;

            return requestedPageNumber;
        }

        public int GetCurrentPageSize(int totalRecords, int requestedPageNumber, int requestedPageSize)
        {
            if (requestedPageSize < 1)
                return 1;
            if (requestedPageSize >= totalRecords)
                return totalRecords;
            else if (requestedPageNumber * requestedPageSize > totalRecords)
                return totalRecords - (totalRecords / requestedPageSize) * requestedPageSize;

            return requestedPageSize;
        }

        public int GetActualPageSize(int totalRecords, int requestedPageNumber, int requestedPageSize)
        {
            if (requestedPageSize < 1)
                return DefaultPageSize;
            if (totalRecords < requestedPageSize)
                return totalRecords;

            return requestedPageSize;
        }

        public int GetSkippedRecords(int totalRecords, int requestedPageNumber, int requestedPageSize)
        {
            return (GetActualPageNumber(totalRecords, requestedPageNumber, requestedPageSize) - 1) * GetActualPageSize(totalRecords, requestedPageNumber, requestedPageSize);
        }

        public int GetTotalPages(int totalRecords, int requestedPageNumber, int requestedPageSize)
        {
            if (totalRecords == 0)
                return 1;
            var actualPageSize = GetActualPageSize(totalRecords, requestedPageNumber, requestedPageSize);
            bool lastPageIsPartial = totalRecords % actualPageSize > 0;

            return totalRecords / actualPageSize + (lastPageIsPartial ? 1 : 0);
        }
    }
}
