﻿namespace Vin.MemberManagement.Infrastructure.Sorting
{
    public enum SortDirection
    {
        ascending = 1,
        descending = 2
    }
}
