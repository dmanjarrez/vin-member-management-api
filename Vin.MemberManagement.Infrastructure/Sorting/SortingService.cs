﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Infrastructure.Sorting
{
    public class SortingService : ISortingService
    {
        public IQueryable<TEntity> Sort<TEntity>(IQueryable<TEntity> items, IList<Sort<TEntity>> sorts)
        {
            if (sorts.Count == 0)
                return items;

            var orderByClause = $"{sorts[0].Property} {sorts[0].Direction}";
            foreach (var s in sorts.Skip(1))
                orderByClause += $", {s.Property} {s.Direction}";

            return items.OrderBy(orderByClause);
        }
    }
}
