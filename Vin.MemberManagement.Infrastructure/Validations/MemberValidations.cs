﻿using System.Text.RegularExpressions;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts.Validations;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Infrastructure
{
    public class MemberValidations : IMemberValidations
    {
        private readonly IValidationResultBuilder _resultBuilder;

        private const string EmailRegex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";

        private const string PhoneRegex = "^\\d{10}$";

        public MemberValidations(IValidationResultBuilder resultBuilder)
        {
            _resultBuilder = resultBuilder;
        }

        public ValidationResult EmailFormattedProperly(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.EmailAddress),
                "Email address is not properly formatted.",
                m => Regex.IsMatch(member.EmailAddress.Value, EmailRegex)
                );
        }

        public ValidationResult EmailUnderSeventyFiveCharacters(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.EmailAddress),
                "Email address length must be 75 characters or fewer.",
                m => m.EmailAddress.Value.Length < 76
                );
        }

        public ValidationResult FirstNameBetweenTwoAndOneHundredCharacters(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.Name.First),
                "First name length must be between 2 and 100 characters.",
                m => m.Name.First.Length < 101 && m.Name.First.Length > 2
                );
        }

        public ValidationResult LastNameBetweenTwoAndOneHundredCharacters(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.Name.Last),
                "Last name length must be between 2 and 100 characters.",
                m => m.Name.Last.Length < 101 && m.Name.Last.Length > 2
                );
        }

        public ValidationResult PhoneNumberFormattedProperly(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.PhoneNumber),
                "Phone number must be 10 digits.",
                m => m.PhoneNumber.Value.Length == 10
                );
        }

        public ValidationResult UserNameLengthBetweenFourAndTwelveCharacters(Member member)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.Username),
                "Last name length must be between 4 and 12 characters.",
                m => m.Username.Value.Length < 13 && m.Username.Value.Length > 3
                );
        }

        public ValidationResult UserNameMustBeUnique(Member member, bool usernameIsUnique)
        {
            return _resultBuilder.Build(
                member,
                nameof(member.Username),
                "Username is already in use by another member.",
                m => usernameIsUnique
                );
        }
    }
}
