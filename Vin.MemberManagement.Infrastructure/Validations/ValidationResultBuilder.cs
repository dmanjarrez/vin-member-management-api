﻿using System;
using Vin.MemberManagement.Infrastructure.Contracts.Validations;

namespace Vin.MemberManagement.Infrastructure.Validations
{
    public class ValidationResultBuilder : IValidationResultBuilder
    {
        public ValidationResult Build<T>(T itemUnderValidation, string propertyName, string failureMessage, Func<T, bool> validationPassesCondition)
        {
            return new ValidationResult
            {
                PropertyName = propertyName,
                FailureMessage = failureMessage,
                IsValid = validationPassesCondition(itemUnderValidation)
            };
        }
    }
}
