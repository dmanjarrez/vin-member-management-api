﻿using System.Collections.Generic;
using System.Linq;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Contracts.Validations;

namespace Vin.MemberManagement.Infrastructure.Validations
{
    public class MemberValidator : IValidator<Member>
    {
        private readonly IMemberValidations _memberValidations;
        private readonly IMemberRepository _memberRepository;
        private readonly IMemberSpecificationFactory _memberSpecFactory;

        public MemberValidator(
            IMemberValidations memberValidations,
            IMemberRepository memberRepository,
            IMemberSpecificationFactory memberSpecFactory)
        {
            _memberValidations = memberValidations;
            _memberRepository = memberRepository;
            _memberSpecFactory = memberSpecFactory;
        }

        public IList<ValidationResult> Validate(Member item)
        {
            bool hasUniqueUsername = false;
            var idSpec = _memberSpecFactory.IdEquals(item.Id);
            var usernameSpec = _memberSpecFactory.UsernameValueEquals(item.Username.Value);

            hasUniqueUsername = !_memberRepository.Get(usernameSpec.AndNot(idSpec)).Any();

            return new List<ValidationResult>
            {
                _memberValidations.EmailFormattedProperly(item),
                _memberValidations.EmailUnderSeventyFiveCharacters(item),
                _memberValidations.FirstNameBetweenTwoAndOneHundredCharacters(item),
                _memberValidations.LastNameBetweenTwoAndOneHundredCharacters(item),
                _memberValidations.PhoneNumberFormattedProperly(item),
                _memberValidations.UserNameLengthBetweenFourAndTwelveCharacters(item),
                _memberValidations.UserNameMustBeUnique(item, hasUniqueUsername)
            };
        }
    }
}
