﻿namespace Vin.MemberManagement.Infrastructure.Validations
{
    public class ValidationResult
    {
        public string PropertyName { get; set; }
        public string FailureMessage { get; set; }
        public bool IsValid { get; set; }
    }
}
