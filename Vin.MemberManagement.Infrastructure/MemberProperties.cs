﻿namespace Vin.MemberManagement.Infrastructure
{
    public static class MemberProperties
    {
        public const string Lastname = "Name.Last";
        public const string Firstname = "Name.First";
        public const string Username = "Username.Value";
        public const string DateOfBirth = "DateOfBirth";
        public const string CreatedDate = "CreatedDate";
        public const string LastUpdatedDate = "LastUpdatedDate";
        public const string IsDeleted = "IsDeleted";
    }
}
