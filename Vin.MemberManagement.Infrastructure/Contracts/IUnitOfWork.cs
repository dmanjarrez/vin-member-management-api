﻿using System;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
