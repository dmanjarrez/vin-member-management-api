﻿using System;
using System.Linq.Expressions;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface ISpecification<TEntity>
    {
        Expression<Func<TEntity, bool>> FuncExpression { get; }
        ISpecification<TEntity> And(ISpecification<TEntity> otherSpec);
        ISpecification<TEntity> AndNot(ISpecification<TEntity> otherSpec);
        ISpecification<TEntity> Or(ISpecification<TEntity> otherSpec);
        ISpecification<TEntity> OrNot(ISpecification<TEntity> otherSpec);
        ISpecification<TEntity> Not();
        bool IsSatisifedBy(TEntity candidate);
    }
}
