﻿using System;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IMemberSpecificationFactory
    {
        ISpecification<Member> IdEquals(Guid id);

        #region Username
        ISpecification<Member> UsernameValueLessThan(string usernameValue);
        ISpecification<Member> UsernameValueLessThanOrEqualTo(string usernameValue);
        ISpecification<Member> UsernameValueEquals(string usernameValue);
        ISpecification<Member> UsernameValueGreaterThan(string usernameValue);
        ISpecification<Member> UsernameValueGreaterThanOrEqualTo(string usernameValue);
        ISpecification<Member> UsernameLike(string usernameValue);
        #endregion

        #region Lastname
        ISpecification<Member> LastnameValueLessThan(string lastname);
        ISpecification<Member> LastnameValueLessThanOrEqualTo(string lastname);
        ISpecification<Member> LastnameValueEquals(string lastname);
        ISpecification<Member> LastnameValueGreaterThan(string lastname);
        ISpecification<Member> LastnameValueGreaterThanOrEqualTo(string lastname);
        ISpecification<Member> LastNameLike(string lastname);
        #endregion

        #region Firstname
        ISpecification<Member> FirstnameValueLessThan(string firstname);
        ISpecification<Member> FirstnameValueLessThanOrEqualTo(string firstname);
        ISpecification<Member> FirstnameValueEquals(string firstname);
        ISpecification<Member> FirstnameValueGreaterThan(string firstname);
        ISpecification<Member> FirstnameValueGreaterThanOrEqualTo(string firstname);
        ISpecification<Member> FirstNameLike(string firstname);
        #endregion

        #region DateOfBirth
        ISpecification<Member> DateOfBirthLessThan(DateTime dateOfBirth);
        ISpecification<Member> DateOfBirthLessThanOrEqualTo(DateTime dateOfBirth);
        ISpecification<Member> DateOfBirthEquals(DateTime dateOfBirth);
        ISpecification<Member> DateOfBirthGreaterThan(DateTime dateOfBirth);
        ISpecification<Member> DateOfBirthGreaterThanOrEqualTo(DateTime dateOfBirth);
        #endregion

        #region EmailAddress
        ISpecification<Member> EmailAddressEquals(string emailAddress);
        ISpecification<Member> EmailAddressLike(string emailAddress);
        #endregion

        #region PhoneNumber
        ISpecification<Member> PhoneNumberEquals(string phoneNumber);
        ISpecification<Member> PhoneNumberLike(string phoneNumber);
        #endregion

        #region CreatedDate
        ISpecification<Member> CreatedDateLessThan(DateTime createdDate);
        ISpecification<Member> CreatedDateLessThanOrEqualTo(DateTime createdDate);
        ISpecification<Member> CreatedDateEquals(DateTime createdDate);
        ISpecification<Member> CreatedDateGreaterThan(DateTime createdDate);
        ISpecification<Member> CreatedDateGreaterThanOrEqualTo(DateTime createdDate);
        #endregion

        #region LastUpdatedDate
        ISpecification<Member> LastUpdatedDateLessThan(DateTime lastUpdatedDate);
        ISpecification<Member> LastUpdatedDateLessThanOrEqualTo(DateTime lastUpdatedDate);
        ISpecification<Member> LastUpdatedDateEquals(DateTime lastUpdatedDate);
        ISpecification<Member> LastUpdatedDateGreaterThan(DateTime lastUpdatedDate);
        ISpecification<Member> LastUpdatedDateGreaterThanOrEqualTo(DateTime lastUpdatedDate);
        #endregion
    }
}
