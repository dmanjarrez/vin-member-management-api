﻿using System;
using System.Collections.Generic;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IRepository<TEntity>
        where TEntity: class 
    {
        TEntity GetByKey(Guid id);

        IList<TEntity> Get(ISpecification<TEntity> specification);

        IList<TEntity> Get(ISpecification<TEntity> specification, IList<Sort<TEntity>> sorts, int pageNumber = 1, int pageSize = 1);

        IList<TEntity> Get(IList<Sort<TEntity>> sorts, int pageNumber = 1, int pageSize = 1);

        void Save(TEntity entity);

        void Delete(Guid id);

        int TotalRecords();

        int TotalRecords(ISpecification<TEntity> specification);
    }
}
