﻿using System.Collections.Generic;
using System.Linq;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface ISortingService
    {
        IQueryable<TEntity> Sort<TEntity>(IQueryable<TEntity> items, IList<Sort<TEntity>> sorts);
    }
}
