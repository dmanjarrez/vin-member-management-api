﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IDatabaseContext
    {
        IDbSet<Member> Members { get; set; }

        int SaveChanges();
        void Dispose();
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry Entry(object entity);
    }
}
