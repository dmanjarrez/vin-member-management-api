﻿using System.Collections.Generic;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Infrastructure.Contracts.Validations
{
    public interface IValidator<T>
    {
        IList<ValidationResult> Validate(T item);
    }
}
