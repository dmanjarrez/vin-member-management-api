﻿using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Infrastructure.Contracts.Validations
{
    public interface IMemberValidations
    {
        ValidationResult UserNameMustBeUnique(Member member, bool usernameIsUnique);
        ValidationResult UserNameLengthBetweenFourAndTwelveCharacters(Member member);
        ValidationResult FirstNameBetweenTwoAndOneHundredCharacters(Member member);
        ValidationResult LastNameBetweenTwoAndOneHundredCharacters(Member member);
        ValidationResult EmailFormattedProperly(Member member);
        ValidationResult EmailUnderSeventyFiveCharacters(Member member);
        ValidationResult PhoneNumberFormattedProperly(Member member);
    }
}
