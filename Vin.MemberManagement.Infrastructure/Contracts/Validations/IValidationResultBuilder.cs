﻿using System;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Infrastructure.Contracts.Validations
{
    public interface IValidationResultBuilder
    {
        ValidationResult Build<T>(T itemUnderValidation, string propertyName, string failureMessage, Func<T, bool> validationPassesCondition);
    }
}
