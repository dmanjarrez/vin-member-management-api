﻿using System.Collections.Generic;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface ISpecificationCombinerService
    {
        ISpecification<TEntity> Union<TEntity>(IList<ISpecification<TEntity>> specifications);
        ISpecification<TEntity> Intersection<TEntity>(IList<ISpecification<TEntity>> specifications);
    }
}
