﻿using System.Linq;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IPagingService
    {
        int DefaultPageSize { get; }

        IQueryable<TEntity> Page<TEntity>(IQueryable<TEntity> items, int totalRecords, int pageNumber, int pageSize);

        int GetActualPageNumber(int totalRecords, int requestedPageNumber, int requestedPageSize);

        int GetCurrentPageSize(int totalRecords, int requestedPageNumber, int requestedPageSize);

        int GetActualPageSize(int totalRecords, int requestedPageNumber, int requestedPageSize);

        int GetSkippedRecords(int totalRecords, int requestedPageNumber, int requestedPageSize);

        int GetTotalPages(int totalRecords, int requestedPageNumber, int requestedPageSize);
    }
}
