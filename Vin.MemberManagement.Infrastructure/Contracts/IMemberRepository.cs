﻿using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Infrastructure.Contracts
{
    public interface IMemberRepository : IRepository<Member>
    {
    }
}
