﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.InfrastructureRepositories
{
    public class RepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        protected readonly IDatabaseContext _context;
        protected readonly ISortingService _sortingService;
        protected readonly IPagingService _pagingService;
        protected const int defaultPageSize = 1;

        public RepositoryBase(
            IDatabaseContext context,
            ISortingService sortingService,
            IPagingService pagingService)
        {
            _context = context;
            _sortingService = sortingService;
            _pagingService = pagingService;
        }

        public virtual void Save(TEntity entity)
        {
            var now = DateTime.Now;

            if (entity.Id == Guid.Empty)
            {
                entity.InitializeId();
                entity.CreatedDate = now;
                _context.Entry(entity).State = EntityState.Added;
            }
            else
                _context.Entry(entity).State = EntityState.Modified;

            entity.LastUpdatedDate = now;
        }

        public virtual IList<TEntity> Get(ISpecification<TEntity> specification)
        {
            return _context.Set<TEntity>()
                .AsNoTracking()
                .Where(specification.FuncExpression.Compile())
                .ToList();
        }

        public virtual IList<TEntity> Get(ISpecification<TEntity> specification, IList<Sort<TEntity>> sorts, int page = 1, int pageSize = 100)
        {
            var items = _context.Set<TEntity>()
                .AsNoTracking()
                .Where(specification.FuncExpression.Compile())
                .AsQueryable();

            var sorted = _sortingService.Sort(items, sorts);

            return _pagingService
                .Page(sorted, TotalRecords(specification), page, pageSize)
                .ToList();
        }

        public virtual IList<TEntity> Get(IList<Sort<TEntity>> sorts, int page = 1, int pageSize = defaultPageSize)
        {
            var items = _context.Set<TEntity>()
                .AsNoTracking()
                .AsQueryable();

            var sorted = _sortingService.Sort(items, sorts);

            return _pagingService
                .Page(sorted, TotalRecords(), page, pageSize)
                .ToList();
        }

        public virtual TEntity GetByKey(Guid id)
        {
            return _context.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefault(e => e.Id == id);
        }

        public void Delete(Guid id)
        {
            var entity = GetByKey(id);
            if (entity == null)
                throw new ArgumentException($"Invalid entity id {id}");
            entity.Delete();
            Save(entity);
        }

        public int TotalRecords()
        {
            return _context.Set<TEntity>()
                .AsNoTracking()
                .Count();
        }

        public int TotalRecords(ISpecification<TEntity> specification)
        {
            return _context.Set<TEntity>()
                .AsNoTracking()
                .Where(specification.FuncExpression.Compile())
                .Count();
        }
    }
}
