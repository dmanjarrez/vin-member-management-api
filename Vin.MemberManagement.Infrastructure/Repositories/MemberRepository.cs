﻿using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.InfrastructureRepositories
{
    public class MemberRepository : RepositoryBase<Member>, IMemberRepository
    {
        public MemberRepository(
            IDatabaseContext context,
            ISortingService sorringService,
            IPagingService pagingService)
            : base(context, sorringService, pagingService)
        { }
    }
}
