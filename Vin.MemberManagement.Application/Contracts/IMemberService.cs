﻿using System;
using System.Collections.Generic;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Sorting;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Application.Contracts
{
    public interface IMemberService
    {
        Member GetById(Guid memberId);

        IList<Member> Get(ISpecification<Member> specification, IList<Sort<Member>> sorts, int pageNumber, int pageSize);

        IList<ValidationResult> Add(Member member);

        IList<ValidationResult> Update(Member member);

        void Delete(Guid id);

        int GetTotalRecords();

        int GetTotalRecords(ISpecification<Member> specification);
    }
}
