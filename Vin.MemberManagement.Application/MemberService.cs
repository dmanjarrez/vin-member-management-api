﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vin.MemberManagement.Application.Contracts;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Contracts.Validations;
using Vin.MemberManagement.Infrastructure.Sorting;
using Vin.MemberManagement.Infrastructure.Validations;

namespace Vin.MemberManagement.Application
{
    public class MemberService : IMemberService
    {
        private readonly IMemberRepository _memberRepository;
        private readonly IUnitOfWork _unitOfwork;
        private readonly IValidator<Member> _memberValidator;

        public MemberService(
            IUnitOfWork unitOfWork,
            IMemberRepository memberRepository,
            IValidator<Member> memberValidator)
        {
            _unitOfwork = unitOfWork;
            _memberRepository = memberRepository;
            _memberValidator = memberValidator;
        }

        public Member GetById(Guid memberId)
        {
            return _memberRepository.GetByKey(memberId);
        }

        public IList<Member> Get(ISpecification<Member> specification, IList<Sort<Member>> sorts, int pageNumber, int pageSize)
        {
            return _memberRepository.Get(specification, sorts, pageNumber, pageSize);
        }

        public IList<ValidationResult> Update(Member member)
        {
            if (member.Id == Guid.Empty)
                throw new InvalidOperationException("A new member cannot be updated.");

            return ValidateAndSave(member);
        }

        public IList<ValidationResult> Add(Member member)
        {
            if (member.Id != Guid.Empty)
                throw new InvalidOperationException("Existing members cannot be added.");

            return ValidateAndSave(member);
        }

        public void Delete(Guid id)
        {
            if (id == Guid.Empty)
                throw new NotImplementedException("New members cannot be deleted.");

            using (_unitOfwork)
            {
                _memberRepository.Delete(id);
                _unitOfwork.Commit();
            }
        }

        public IList<ValidationResult> ValidateAndSave(Member member)
        {
            var results = _memberValidator.Validate(member);

            if (results.Any(r => !r.IsValid))
                return results;

            using (_unitOfwork)
            {
                _memberRepository.Save(member);
                _unitOfwork.Commit();
            }

            return results;
        }

        public int GetTotalRecords()
        {
            return _memberRepository.TotalRecords();
        }

        public int GetTotalRecords(ISpecification<Member> specification)
        {
            return _memberRepository.TotalRecords(specification);
        }
    }
}
