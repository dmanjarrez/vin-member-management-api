﻿using System;

namespace Vin.MemberManagement.Domain
{
    public class Name
    {
        public string First { get; protected set; }
        public string Last { get; protected set; }

        protected Name() { }

        public Name(string first, string last)
        {
            if (string.IsNullOrWhiteSpace(first) || string.IsNullOrWhiteSpace(last))
                throw new ArgumentException("First and last name are required.");

            Last = last;
            First = first;
        }
    }
}
