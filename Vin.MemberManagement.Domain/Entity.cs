﻿
using System;

namespace Vin.MemberManagement.Domain
{
    public abstract class Entity
    {
        public Guid Id { get; protected set; }
        public bool IsDeleted { get; protected set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public void Delete()
        {
            IsDeleted = true;
        }

        public void InitializeId()
        {
            Id = Guid.NewGuid();
        }
    }
}
