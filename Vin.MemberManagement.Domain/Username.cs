﻿using System;

namespace Vin.MemberManagement.Domain
{
    public class Username
    {
        public string Value { get; protected set; }

        protected Username() { }

        public Username(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            Value = value;
        }
    }
}
