﻿using System;

namespace Vin.MemberManagement.Domain
{

    public class PhoneNumber
    {

        public string Value { get; protected set; }

        protected PhoneNumber() { }

        public PhoneNumber(string value)
        {
            if(string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            Value = value;
        }
    }
}
