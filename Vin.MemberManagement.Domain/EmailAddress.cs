﻿using System;
using System.Text.RegularExpressions;

namespace Vin.MemberManagement.Domain
{
    public class EmailAddress
    {
        public string Value { get; protected set; }

        protected EmailAddress() { }

        public EmailAddress(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));
            Value = value;
        }
    }
}
