﻿using System;

namespace Vin.MemberManagement.Domain
{
    public class Member : Entity
    {

        protected Member() { }

        public virtual Username Username { get; set; }

        public virtual Name Name { get; set; }

        public virtual DateTime DateOfBirth { get; set; }

        public virtual EmailAddress EmailAddress { get; set; }

        public virtual PhoneNumber PhoneNumber { get; set; }

        public Member(Username username, Name name, DateTime dateOfBirth, EmailAddress emailAddress, PhoneNumber phoneNumber)
        {
            Username = username;
            Name = name;
            DateOfBirth = dateOfBirth;
            EmailAddress = emailAddress;
            PhoneNumber = phoneNumber;
            Id = Guid.NewGuid();
        }
    }
}
