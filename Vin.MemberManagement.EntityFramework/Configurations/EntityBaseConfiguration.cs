﻿using System.Data.Entity.ModelConfiguration;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.EntityFramework.Configurations
{
    public class EntityBaseConfiguration<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity: Entity
    {
        public EntityBaseConfiguration(string primaryKeyColumnName = "Id")
        {
            HasKey(x => x.Id);

            Property(x => x.Id)
                .IsRequired()
                .HasColumnName(primaryKeyColumnName);

            Property(x => x.CreatedDate).IsRequired();

            Property(x => x.LastUpdatedDate).IsRequired();

            Property(x => x.IsDeleted).IsRequired();
        }
    }
}
