﻿using System;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.EntityFramework.Configurations
{
    public class MemberConfiguration : EntityBaseConfiguration<Member>
    {
        public MemberConfiguration()
            : base("MemberId")
        {
            ToTable("Member");

            Property(x => x.Name.First)
                .IsRequired()
                .HasColumnName("Firstname")
                .HasMaxLength(100);

            Property(x => x.Name.Last)
                .IsRequired()
                .HasColumnName("Lastname")
                .HasMaxLength(100);

            Property(x => x.Username.Value)
                .IsRequired()
                .HasColumnName("Username")
                .HasMaxLength(12);

            Property(x => x.PhoneNumber.Value)
                .IsOptional()
                .HasColumnName("PhoneNumber")
                .IsFixedLength()
                .HasMaxLength(10);

            Property(x => x.EmailAddress.Value)
                .IsRequired()
                .HasColumnName("EmailAddress")
                .IsFixedLength()
                .HasMaxLength(75);

            Property(x => x.DateOfBirth)
                .IsRequired();

        }
    }
}
