﻿using System;
using Vin.MemberManagement.Infrastructure;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.EntityFramework
{
    public class EntityFrameworkUnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly IDatabaseContext _context;

        public EntityFrameworkUnitOfWork(IDatabaseContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            if (_disposed)
                throw new ObjectDisposedException(this.GetType().FullName);

            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_disposed) return;

            if(isDisposing && _context != null)
            {
                //_context.Dispose();
            }

            _disposed = true;
        }
    }
}
