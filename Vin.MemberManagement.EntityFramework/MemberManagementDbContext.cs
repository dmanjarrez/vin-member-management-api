﻿using System.Data.Entity;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.EntityFramework.Configurations;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.EntityFramework
{
    public class MemberManagementDbContext : DbContext, IDatabaseContext
    {
        public virtual IDbSet<Member> Members { get; set; }

        public MemberManagementDbContext()
        { }

        public MemberManagementDbContext(string connectionString)
            : base(connectionString)
        {
        }

        public override DbSet<TEntity> Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MemberConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
