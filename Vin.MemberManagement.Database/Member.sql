﻿CREATE TABLE [dbo].[Member]
(
	[MemberId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [LastName] VARCHAR(100) NOT NULL, 
    [FirstName] VARCHAR(100) NOT NULL, 
    [Username] VARCHAR(12) NOT NULL, 
    [EmailAddress] VARCHAR(100) NOT NULL, 
    [PhoneNumber] CHAR(10) NULL, 
    [DateOfBirth] DATETIME2 NOT NULL, 
    [CreatedDate] DATETIME2 NOT NULL, 
    [LastUpdatedDate] DATETIME2 NOT NULL, 
    [IsDeleted] BIT NOT NULL DEFAULT 0
)
