﻿using System.Collections.Generic;
using System.Linq;

namespace Vin.MemberManagement.Api.Services
{
    public static class MemberProperties
    {
        public const string Username = "Username";
        public const string Lastname = "Lastname";
        public const string Firstname = "Firstname";
        public const string DateOfBirth = "DateOfBirth";
        public const string EmailAddress = "EmailAddress";
        public const string PhoneNumber = "PhoneNumber";
        public const string CreatedDate = "CreatedDate";
        public const string LastUpdatedDate = "LastUpdatedDate";
        public const string IsDeleted = "IsDeleted";

        public static string[] GetSortable()
        {
            return new[]
            {
                Username,
                Lastname,
                Firstname,
                DateOfBirth,
                CreatedDate,
                LastUpdatedDate,
                IsDeleted
            };
        }

        public static IDictionary<string, string[]> GetFilterable()
        {
            return new Dictionary<string, string[]>
            {
                { Username, GetStringNumericComparers() },
                { Lastname, GetStringNumericComparers() },
                { Firstname, GetStringNumericComparers() },
                { DateOfBirth, GetDateComparers() },
                { EmailAddress, GetNonOrderableStringComparers() },
                { PhoneNumber, GetNonOrderableStringComparers() },
                { CreatedDate, GetDateComparers() },
                { LastUpdatedDate, GetDateComparers() },
                { IsDeleted, GetBooleanComparers() }
            };
        }

        private static string[] GetAllComparers()
        {
            return new[] {
                FilterComparers.eq.ToString(),
                FilterComparers.gt.ToString(),
                FilterComparers.gteq.ToString(),
                FilterComparers.like.ToString(),
                FilterComparers.lt.ToString(),
                FilterComparers.lteq.ToString(),
                FilterComparers.neq.ToString()
            };
        }

        private static string[] GetStringNumericComparers()
        {
            return GetAllComparers();
        }

        private static string[] GetNonOrderableStringComparers()
        {
            return GetAllComparers().Where(c =>
                    c != FilterComparers.lt.ToString()
                    && c != FilterComparers.lteq.ToString()
                    && c != FilterComparers.gteq.ToString()
                    && c != FilterComparers.gt.ToString()).ToArray();
        }

        private static string[] GetDateComparers()
        {
            return GetAllComparers().Where(c => c != FilterComparers.like.ToString()).ToArray();
        }

        private static string[] GetBooleanComparers()
        {
            return GetAllComparers().Where(c => c != FilterComparers.like.ToString()).ToArray();
        }
    }
}