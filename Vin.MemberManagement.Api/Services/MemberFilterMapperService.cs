﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Api.Services
{
    public class MemberFilterMapperService : IFilterMapperService<Member>
    {
        private readonly IMemberSpecificationFactory _specFactory;
        private readonly ISpecificationCombinerService _specCombinerService;

        public MemberFilterMapperService(
            IMemberSpecificationFactory specFactory,
            ISpecificationCombinerService specCombinerService)
        {
            _specFactory = specFactory;
            _specCombinerService = specCombinerService;
        }

        public ISpecification<Member> GetSpecificationForFiltersUnion(IList<FilterablePropertyModel> filters)
        {
            return _specCombinerService.Union(filters.Select(f => GetSpecification(f)).ToList());
        }

        public ISpecification<Member> GetSpecificationForFiltersIntersection(IList<FilterablePropertyModel> filters)
        {
            return _specCombinerService.Intersection(filters.Select(f => GetSpecification(f)).ToList());
        }

        public ISpecification<Member> GetSpecification(FilterablePropertyModel filterModel)
        {
            var comparedValue = filterModel.ComparedValue;

            Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>> maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>();

            switch (filterModel.PropertyName)
            {
                case MemberProperties.DateOfBirth:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.DateOfBirth, FilterComparers.lt), _specFactory.DateOfBirthLessThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.DateOfBirth, FilterComparers.lteq), _specFactory.DateOfBirthLessThanOrEqualTo(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.DateOfBirth, FilterComparers.eq), _specFactory.DateOfBirthEquals(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.DateOfBirth, FilterComparers.gt), _specFactory.DateOfBirthGreaterThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.DateOfBirth, FilterComparers.gteq), _specFactory.DateOfBirthGreaterThanOrEqualTo(DateTime.Parse(comparedValue)) }
                    };
                    break;
                case MemberProperties.Lastname:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.lt), _specFactory.LastnameValueLessThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.lteq), _specFactory.LastnameValueLessThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.eq), _specFactory.LastnameValueEquals(comparedValue) },
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.gt), _specFactory.LastnameValueGreaterThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.gteq), _specFactory.LastnameValueGreaterThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Lastname, FilterComparers.like), _specFactory.LastNameLike(comparedValue) }
                    };
                    break;
                case MemberProperties.Firstname:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.lt), _specFactory.FirstnameValueLessThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.lteq), _specFactory.FirstnameValueLessThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.eq), _specFactory.FirstnameValueEquals(comparedValue) },
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.gt), _specFactory.FirstnameValueGreaterThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.gteq), _specFactory.FirstnameValueGreaterThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Firstname, FilterComparers.like), _specFactory.FirstNameLike(comparedValue) }
                    };
                    break;
                case MemberProperties.Username:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.Username, FilterComparers.lt), _specFactory.UsernameValueLessThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Username, FilterComparers.lteq), _specFactory.UsernameValueLessThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Username, FilterComparers.eq), _specFactory.UsernameValueEquals(comparedValue) },
                        { Tuple.Create(MemberProperties.Username, FilterComparers.gt), _specFactory.UsernameValueGreaterThan(comparedValue) },
                        { Tuple.Create(MemberProperties.Username, FilterComparers.gteq), _specFactory.UsernameValueGreaterThanOrEqualTo(comparedValue) },
                        { Tuple.Create(MemberProperties.Username, FilterComparers.like), _specFactory.UsernameLike(comparedValue) }
                    };
                    break;
                case MemberProperties.EmailAddress:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.EmailAddress, FilterComparers.eq), _specFactory.EmailAddressEquals(comparedValue) },
                        { Tuple.Create(MemberProperties.EmailAddress, FilterComparers.like), _specFactory.EmailAddressLike(comparedValue) }
                    };
                    break;
                case MemberProperties.PhoneNumber:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.PhoneNumber, FilterComparers.eq), _specFactory.PhoneNumberEquals(comparedValue) },
                        { Tuple.Create(MemberProperties.PhoneNumber, FilterComparers.like), _specFactory.PhoneNumberLike(comparedValue) }
                    };
                    break;
                case MemberProperties.CreatedDate:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.CreatedDate, FilterComparers.lt), _specFactory.CreatedDateLessThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.CreatedDate, FilterComparers.lteq), _specFactory.CreatedDateLessThanOrEqualTo(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.CreatedDate, FilterComparers.eq), _specFactory.CreatedDateEquals(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.CreatedDate, FilterComparers.gt), _specFactory.CreatedDateGreaterThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.CreatedDate, FilterComparers.gteq), _specFactory.CreatedDateGreaterThanOrEqualTo(DateTime.Parse(comparedValue)) }
                    };
                    break;
                case MemberProperties.LastUpdatedDate:
                    maps = new Dictionary<Tuple<string, FilterComparers>, ISpecification<Member>>
                    {
                        { Tuple.Create(MemberProperties.LastUpdatedDate, FilterComparers.lt), _specFactory.LastUpdatedDateLessThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.LastUpdatedDate, FilterComparers.lteq), _specFactory.LastUpdatedDateLessThanOrEqualTo(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.LastUpdatedDate, FilterComparers.eq), _specFactory.LastUpdatedDateEquals(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.LastUpdatedDate, FilterComparers.gt), _specFactory.LastUpdatedDateGreaterThan(DateTime.Parse(comparedValue)) },
                        { Tuple.Create(MemberProperties.LastUpdatedDate, FilterComparers.gteq), _specFactory.LastUpdatedDateGreaterThanOrEqualTo(DateTime.Parse(comparedValue)) }
                    };
                    break;
                default:
                    throw new FilterMapException($"Invalid filterable property for Member entity: {filterModel.PropertyName}.", "Member", filterModel);
            }

            return maps[Tuple.Create(filterModel.PropertyName, EnumParser.Parse(filterModel.Comparer, FilterComparers.eq))];
        }
    }
}