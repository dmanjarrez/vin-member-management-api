﻿using System;

namespace Vin.MemberManagement.Api.Services
{
    public static class EnumParser
    {
        public static TEnum Parse<TEnum>(string enumStringValue, TEnum defaultValue)
            where TEnum: struct
        {
            TEnum parsed;
            if (Enum.TryParse(enumStringValue, out parsed))
                return parsed;

            return defaultValue;
        }
    }
}