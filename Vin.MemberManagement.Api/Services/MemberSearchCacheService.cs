﻿using System;
using System.Runtime.Caching;
using Vin.MemberManagement.Api.Models;

namespace Vin.MemberManagement.Api.Services
{
    public class MemberSearchCacheService : IMemberSearchCacheService
    {
        private readonly MemoryCache _cache;
        private readonly object _lock;

        public MemberSearchCacheService()
        {
            _cache = new MemoryCache("MemberSearch");
            _lock = new object();
        }

        public void SetSearchPages(Guid userId, MemberSearchResultModel model)
        {
            var previous = BuildPreviousPageSearch(userId, model);
            var next = BuildNextPageSearch(userId, model);

            SetPageModel(userId, RelativePage.Current, model.Search);
            if(previous != null)
                SetPageModel(userId, RelativePage.Previous, previous);
            if(next != null)
                SetPageModel(userId, RelativePage.Next, next);
        }

        public bool SetPreviousPage(Guid userId, MemberSearchResultModel model)
        {
            var previous = BuildPreviousPageSearch(userId, model);

            if (previous != null)
            {
                SetPageModel(userId, RelativePage.Previous, previous);
                return true;
            }

            return false;
        }

        public bool SetCurrentPage(Guid userId, MemberSearchResultModel model)
        {
            SetPageModel(userId, RelativePage.Current, model.Search);
            return true;
        }

        public bool SetNextPage(Guid userId, MemberSearchResultModel model)
        {
            var previous = BuildNextPageSearch(userId, model);

            if (previous != null)
            {
                SetPageModel(userId, RelativePage.Next, previous);
                return true;
            }

            return false;
        }

        public MemberSearchModel GetPageModel(Guid userId, RelativePage relativePage)
        {
            var key = GetKey(userId, relativePage);
            return (MemberSearchModel)_cache[key];
        }

        public void SetPageModel(Guid userId, RelativePage relativePage, MemberSearchModel searchModel)
        {
            var key = GetKey(userId, relativePage);
            lock(_lock)
            {
                if (_cache.Contains(key))
                    _cache[key] = searchModel;
                else
                    _cache.Add(key, searchModel, new CacheItemPolicy());
            }
        }

        public MemberSearchModel BuildPreviousPageSearch(Guid userId, MemberSearchResultModel model)
        {
            return model.Paging.PageNumber > 1
                ? new MemberSearchModel
                {
                    Filtering = model.Filters,
                    Sorting = model.Sorting,
                    PageNumber = model.Paging.PageNumber - 1,
                    PageSize = model.Paging.MaxPageSize
                } : null;
        }

        public MemberSearchModel BuildNextPageSearch(Guid userId, MemberSearchResultModel model)
        {
            return model.Paging.PageNumber < model.Paging.TotalPages
                ? new MemberSearchModel
                {
                    Filtering = model.Filters,
                    Sorting = model.Sorting,
                    PageNumber = model.Paging.PageNumber + 1,
                    PageSize = model.Paging.MaxPageSize
                } : null;
        }

        private string GetKey(Guid userId, RelativePage relativePage)
        {
            return userId + relativePage.ToString();
        }
    }
}
