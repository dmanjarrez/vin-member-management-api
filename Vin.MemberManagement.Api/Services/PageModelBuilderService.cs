﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Api.Services
{
    public class PageModelBuilderService : IPageModelBuilderService
    {
        private readonly IPagingService _pagingService;
        private readonly IMemberSearchCacheService _memberSearchCacheService;
        private readonly UrlHelper _urlHelper;

        public PageModelBuilderService(
            IPagingService pagingService,
            IMemberSearchCacheService memberSearchCacheService)
        {
            _pagingService = pagingService;
            _memberSearchCacheService = memberSearchCacheService;
            _urlHelper = new UrlHelper(HttpContext.Current.Items["MS_HttpRequestMessage"] as HttpRequestMessage);
        }

        public PageModel BuildPageModel(int requestedPageNumber, int requestedPageSize, int totalRecords)
        {
            return new PageModel
            {
                PageNumber = _pagingService.GetActualPageNumber(totalRecords, requestedPageNumber, requestedPageSize),
                MaxPageSize = _pagingService.GetActualPageSize(totalRecords, requestedPageNumber, requestedPageSize),
                CurrentPageSize = _pagingService.GetCurrentPageSize(totalRecords, requestedPageNumber, requestedPageSize),
                TotalPages = _pagingService.GetTotalPages(totalRecords, requestedPageNumber, requestedPageSize),
                TotalRecords = totalRecords
            };
        }

        public void SetPreviousAndNextPageUris(MemberSearchResultModel searchResult)
        {
            _memberSearchCacheService.SetCurrentPage(Guid.Empty, searchResult);
            searchResult.Paging.CurrentPageUri = _urlHelper.Link(ActionNames.MemberSearchCurrentPage, new { userId = Guid.Empty });

            searchResult.Paging.PreviousPageUri = _memberSearchCacheService.SetPreviousPage(Guid.Empty, searchResult)
                ? _urlHelper.Link(ActionNames.MemberSearchPreviousPage, new { userId = Guid.Empty })
                : "";

            searchResult.Paging.NextPageUri = _memberSearchCacheService.SetNextPage(Guid.Empty, searchResult)
                ? _urlHelper.Link(ActionNames.MemberSearchNextPage, new { userId = Guid.Empty })
                : "";
        }
    }
}