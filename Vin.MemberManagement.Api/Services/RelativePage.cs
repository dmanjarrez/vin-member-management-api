﻿namespace Vin.MemberManagement.Api.Services
{
    public enum RelativePage
    {
        Current = 0,
        Previous = 1,
        Next = 2
    }
}
