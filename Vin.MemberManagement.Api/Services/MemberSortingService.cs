﻿using System.Collections.Generic;
using System.Linq;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Api.Services
{
    public class MemberSortingService : ISortingService<Member>
    {
        private Dictionary<string, string> apiToDomainPropertiesMap = new Dictionary<string, string>
        {
            { MemberProperties.Lastname, Infrastructure.MemberProperties.Lastname },
            { MemberProperties.Firstname, Infrastructure.MemberProperties.Firstname },
            { MemberProperties.DateOfBirth, Infrastructure.MemberProperties.DateOfBirth },
            { MemberProperties.Username, Infrastructure.MemberProperties.Username },
            { MemberProperties.CreatedDate, Infrastructure.MemberProperties.CreatedDate },
            { MemberProperties.LastUpdatedDate, Infrastructure.MemberProperties.LastUpdatedDate },
            { MemberProperties.IsDeleted, Infrastructure.MemberProperties.IsDeleted }
        };

        public IList<Sort<Member>> MapSorts(IList<SortablePropertyModel> apiSorts)
        {
            if (apiSorts == null || !apiSorts.Any())
                return new List<Sort<Member>> {
                    new Sort<Member> { Property = apiToDomainPropertiesMap[MemberProperties.Lastname], Direction = SortDirection.ascending }
                };

            return apiSorts.Select(s => {
                if (!apiToDomainPropertiesMap.ContainsKey(s.PropertyName))
                    throw new SortingMapException($"Invalid sortable property for Member entity: {s.PropertyName}.", "Member", s);
                return new Sort<Member>
                {
                    Property = apiToDomainPropertiesMap[s.PropertyName],
                    Direction = EnumParser.Parse(s.Direction, SortDirection.ascending)
                };
            }).ToList();
        }
    }
}