﻿using System.Collections.Generic;

namespace Vin.MemberManagement.Api.Models
{
    public class MembersModel
    {
        public IList<MemberModel> Members { get; set; }
        public IList<SortablePropertyModel> Sorting { get; set; }
        public IList<FilterablePropertyModel> Filtering { get; set; }
        public PageModel Paging { get; set; }
    }
}