﻿using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Api.Models
{
    public class SortablePropertyModel
    {
        public string PropertyName { get; set; }
        public string Direction { get; set; }
    }
}