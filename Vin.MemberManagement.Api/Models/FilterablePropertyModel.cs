﻿namespace Vin.MemberManagement.Api.Models
{
    public class FilterablePropertyModel
    {
        public string PropertyName { get; set; }
        public string Comparer { get; set; }
        public string ComparedValue { get; set; }
    }
}