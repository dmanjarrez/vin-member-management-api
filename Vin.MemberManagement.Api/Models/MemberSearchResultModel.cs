﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Vin.MemberManagement.Api.Models
{
    [KnownType(typeof(IList<MemberModel>))]
    [KnownType(typeof(PageModel))]
    [KnownType(typeof(IList<SortablePropertyModel>))]
    [KnownType(typeof(IList<FilterablePropertyModel>))]
    [KnownType(typeof(MemberSearchModel))]
    public class MemberSearchResultModel
    {
        public IList<MemberModel> Members { get; set; }
        public PageModel Paging { get; set; }
        public IList<SortablePropertyModel> Sorting { get; set; }
        public IList<FilterablePropertyModel> Filters { get; set; }
        public MemberSearchModel Search { get; set; }
    }
}