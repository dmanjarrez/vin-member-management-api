﻿namespace Vin.MemberManagement.Api.Models
{
    public class PageModel
    {
        public int? PageNumber { get; set; }
        public int? MaxPageSize { get; set; }
        public int CurrentPageSize { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public string CurrentPageUri { get; set; }
        public string PreviousPageUri { get; set; }
        public string NextPageUri { get; set; }
    }
}