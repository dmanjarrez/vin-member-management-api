﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Vin.MemberManagement.Api.Models
{
    [KnownType(typeof(IList<SortablePropertyModel>))]
    [KnownType(typeof(IList<FilterablePropertyModel>))]
    public class MemberSearchModel
    {
        public IList<SortablePropertyModel> Sorting { get; set; }
        public IList<FilterablePropertyModel> Filtering { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }

        public MemberSearchModel()
        {
            Sorting = new List<SortablePropertyModel>();
            Filtering = new List<FilterablePropertyModel>();
        }
    }
}