﻿using AutoMapper;
using Ninject;
using Ninject.Modules;
using System;
using System.Linq;
using System.Reflection;
using Vin.MemberManagement.Api.Services;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Api.Ninject
{
    public class MainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPageModelBuilderService>().To<PageModelBuilderService>();
            Bind<IFilterMapperService<Member>>().To<MemberFilterMapperService>();
            Bind<ISortingService<Member>>().To<MemberSortingService>();
            Bind<IMemberSearchCacheService>().To<MemberSearchCacheService>().InSingletonScope();

            try
            {
                Bind<IMapper>().ToMethod((context) =>
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.ConstructServicesUsing(type => context.Kernel.Get(type));

                        var assembly = Assembly.GetAssembly(typeof(MainModule));
                        foreach (var profileType in assembly.GetTypes().Where(t => typeof(Profile).IsAssignableFrom(t)))
                        {
                            cfg.AddProfile(Activator.CreateInstance(profileType) as Profile);
                        }
                    });

                    config.AssertConfigurationIsValid();

                    return config.CreateMapper();
                }).InSingletonScope();
            }
            catch(Exception ex)
            {
                var a = ex;
            }
        }
    }
}