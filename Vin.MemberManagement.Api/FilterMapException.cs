﻿using System;
using System.Runtime.Serialization;
using Vin.MemberManagement.Api.Models;

namespace Vin.MemberManagement.Api
{
    public class FilterMapException : Exception
    {
        public FilterablePropertyModel FilterModel { get; }
        public string EntityName { get; }

        public FilterMapException()
        {
        }

        public FilterMapException(string message) : base(message)
        {
        }

        public FilterMapException(string message, string entityName, FilterablePropertyModel filterModel)
            : base(message)
        {
            FilterModel = FilterModel;
            EntityName = entityName;
        }

        public FilterMapException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FilterMapException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}