﻿namespace Vin.MemberManagement.Api
{
    public static class ActionNames
    {
        public const string MemberSearchPreviousPage = "memberSearchPreviousPage";
        public const string MemberSearchNextPage = "memberSearchNextPage";
        public const string MemberSearchCurrentPage = "memberSearchCurrentPage";
        public const string MemberSearchPost = "memberSearchPost";
    }
}