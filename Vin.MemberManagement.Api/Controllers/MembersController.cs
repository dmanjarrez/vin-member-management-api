﻿using AutoMapper;
using System;
using System.Linq;
using System.Web.Http;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Application.Contracts;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Api.Controllers
{
    [RoutePrefix("api/members")]
    public class MembersController : BaseApiController
    {
        private readonly IMemberService _memberService;
        private readonly IMapper _mapper;

        public MembersController(
            IMemberService memberService,
            IMapper mapper)
        {
            _memberService = memberService;
            _mapper = mapper;
        }

        public JsonResult Get(Guid id)
        {
            return ExecuteAndCatch(() =>
            {
                var member = _mapper.Map<Member, MemberModel>(_memberService.GetById(id));

                return JsonResult.BuildSuccess(
                    new { Member = member },
                    "Success");
            });
        }

        public JsonResult Post(MemberModel model)
        {
            return ExecuteAndCatch(() =>
            {
                var member = _mapper.Map<MemberModel, Member>(model);

                var failures = _memberService.Add(member).Where(r => !r.IsValid).ToList();

                if (failures.Any())
                    return JsonResult.BuildFailure(new
                    {
                        ValidationFailures = failures,
                        Member = model
                    },
                    "The member failed one or more validations.");
                else
                    return JsonResult.BuildSuccess(model, "Success");
            });
        }

        public JsonResult Put(MemberModel model)
        {
            return ExecuteAndCatch(() =>
            {
                var member = _mapper.Map<MemberModel, Member>(model);

                var failures = _memberService.Update(member).Where(r => !r.IsValid).ToList();

                if (failures.Any())
                    return JsonResult.BuildFailure(new
                    {
                        ValidationFailures = failures,
                        Member = model
                    },
                    "The member failed one or more validations.");
                else
                    return JsonResult.BuildSuccess(model, "Success");
            });
        }

        public JsonResult Delete(Guid id)
        {
            return ExecuteAndCatch(() =>
            {
                _memberService.Delete(id);
                return JsonResult.BuildSuccess(
                    null,
                    $"Member {id} deleted");
            });
        }
    }
}
