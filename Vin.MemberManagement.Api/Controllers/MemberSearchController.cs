﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Api.Services;
using Vin.MemberManagement.Application.Contracts;
using Vin.MemberManagement.Domain;
using Vin.MemberManagement.Infrastructure.Contracts;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Api.Controllers
{
    [RoutePrefix("api/memberSearch")]
    public class MemberSearchController : BaseApiController
    {
        private readonly IMemberService _memberService;
        private readonly IMapper _mapper;
        private readonly IPagingService _pagingService;
        private readonly IPageModelBuilderService _pageModelBuilder;
        private readonly IFilterMapperService<Member> _memberFilterMapperService;
        private readonly ISortingService<Member> _memberSortingService;
        private readonly IMemberSearchCacheService _memberSearchCacheService;

        public MemberSearchController(
            IMemberService memberService,
            IMapper mapper,
            IPagingService pagingService,
            IPageModelBuilderService pageModelBuilder,
            IFilterMapperService<Member> memberFilterMapperService,
            ISortingService<Member> memberSortingService,
            IMemberSearchCacheService memberSearchCacheService)
        {
            _memberService = memberService;
            _mapper = mapper;
            _pagingService = pagingService;
            _pageModelBuilder = pageModelBuilder;
            _memberFilterMapperService = memberFilterMapperService;
            _memberSortingService = memberSortingService;
            _memberSearchCacheService = memberSearchCacheService;
        }

        [HttpPost]
        [Route("", Name = "memberSearchPost")]
        public JsonResult Post(MemberSearchModel model)
        {
            return ExecuteAndCatch(() =>
            {
                var resultModel = DoSearch(model);

                return JsonResult.BuildSuccess(
                    resultModel,
                    "Success");
            });
        }

        [HttpGet]
        [Route("sortableProperties")]
        public JsonResult GetSortableProperties()
        {
            return ExecuteAndCatch(() =>
            {
                return JsonResult.BuildSuccess(
                    new { SortableProperties = MemberProperties.GetSortable() },
                    "Succcess");
            });
        }

        [HttpGet]
        [Route("filterableProperties")]
        public JsonResult GetFilterableProperties()
        {
            return ExecuteAndCatch(() =>
            {
                return JsonResult.BuildSuccess(
                    new { FilterableProperties = MemberProperties.GetFilterable() },
                    "Succcess");
            });
        }

        [HttpGet]
        [Route("sortDirections")]
        public JsonResult GetSortDirections()
        {
            return ExecuteAndCatch(() =>
            {
                return JsonResult.BuildSuccess(
                    new { SortDirections = Enum.GetNames(typeof(SortDirection)) },
                    "Succcess");
            });
        }

        [HttpGet]
        [Route("filterComparers")]
        public JsonResult GetFilterComparers()
        {
            return ExecuteAndCatch(() =>
            {
                return JsonResult.BuildSuccess(
                    new { FilterComparers = Enum.GetNames(typeof(FilterComparers)) },
                    "Succcess");
            });
        }

        private MemberSearchResultModel DoSearch(MemberSearchModel searchModel)
        {
            var domainSorts = _memberSortingService.MapSorts(searchModel.Sorting);
            var filterSpecifications = _memberFilterMapperService.GetSpecificationForFiltersUnion(searchModel.Filtering);
            var members = _mapper.Map<IList<Member>, IList<MemberModel>>(_memberService.Get(filterSpecifications, domainSorts, searchModel.PageNumber ?? 1, searchModel.PageSize ?? _pagingService.DefaultPageSize));

            var searchResult = new MemberSearchResultModel
            {
                Search = searchModel,
                Members = members,
                Sorting = searchModel.Sorting,
                Filters = searchModel.Filtering,
                Paging = _pageModelBuilder.BuildPageModel(
                            searchModel.PageNumber ?? 1,
                            searchModel.PageSize ?? _pagingService.DefaultPageSize,
                            _memberService.GetTotalRecords(filterSpecifications))
            };

            // I didn't get this working as well as I wanted
            //_pageModelBuilder.SetPreviousAndNextPageUris(searchResult);

            return searchResult;
        }
    }
}
