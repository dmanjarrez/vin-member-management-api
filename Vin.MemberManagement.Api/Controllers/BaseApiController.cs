﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Vin.MemberManagement.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class BaseApiController : ApiController
    {
        protected JsonResult ExecuteAndCatch(Func<JsonResult> function)
        {
            try
            {
                return function();
            }

            catch (FilterMapException fe)
            {
                return JsonResult.BuildFailure(fe.FilterModel, fe.Message);
            }
            catch (SortingMapException se)
            {
                return JsonResult.BuildFailure(se.SortModel, se.Message);
            }
            catch (Exception e)
            {
                // _logger.log(e);
                Console.Error.WriteLine(e);
                return JsonResult.BuildFailure(null, "An unexpected error occurred.");
            }
        }
    }
}
