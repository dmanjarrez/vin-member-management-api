﻿using System;

namespace Vin.MemberManagement.Api
{
    public class JsonResult
    {
        public static JsonResult BuildSuccess(object data, string message)
        {
            return new JsonResult
            {
                Code = 200,
                Data = data,
                Message = message,
                Success = true,
                Timestamp = DateTime.Now.ToUniversalTime()
            };
        }

        public static JsonResult BuildFailure (object data, string message)
        {
            return new JsonResult
            {
                Code = 200,
                Data = data,
                Message = message,
                Timestamp = DateTime.Now.ToUniversalTime()
            };
        }

        public DateTime Timestamp { get; set; }
        public int Code { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}