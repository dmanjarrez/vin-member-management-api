﻿using Vin.MemberManagement.Api.Models;

namespace Vin.MemberManagement.Api.Services
{
    public interface IPageModelBuilderService
    {
        PageModel BuildPageModel(int requestedPageNumber, int requestedPageSize, int totalRecords);
        void SetPreviousAndNextPageUris(MemberSearchResultModel searchResulstModel);
    }
}