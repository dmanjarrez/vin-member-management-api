﻿using System;
using Vin.MemberManagement.Api.Models;

namespace Vin.MemberManagement.Api.Services
{
    public interface IMemberSearchCacheService
    {
        bool SetCurrentPage(Guid userId, MemberSearchResultModel searchModel);
        bool SetPreviousPage(Guid userId, MemberSearchResultModel searchModel);
        bool SetNextPage(Guid userId, MemberSearchResultModel searchModel);
        MemberSearchModel GetPageModel(Guid userId, RelativePage relativePage);
    }
}
