﻿using System.Collections.Generic;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Infrastructure.Contracts;

namespace Vin.MemberManagement.Api.Services
{
    public interface IFilterMapperService<TEntity>
    {
        ISpecification<TEntity> GetSpecification(FilterablePropertyModel filterModel);

        ISpecification<TEntity> GetSpecificationForFiltersUnion(IList<FilterablePropertyModel> filters);

        ISpecification<TEntity> GetSpecificationForFiltersIntersection(IList<FilterablePropertyModel> filters);
    }
}