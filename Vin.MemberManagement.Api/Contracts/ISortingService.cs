﻿using System.Collections.Generic;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Infrastructure.Sorting;

namespace Vin.MemberManagement.Api.Services
{
    public interface ISortingService<TEntity>
    {
        IList<Sort<TEntity>> MapSorts(IList<SortablePropertyModel> apiSorts);
    }
}
