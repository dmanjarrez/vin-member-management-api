﻿using System;
using System.Runtime.Serialization;
using Vin.MemberManagement.Api.Models;

namespace Vin.MemberManagement.Api
{
    public class SortingMapException : Exception
    {
        public SortablePropertyModel SortModel { get; }
        public string EntityName { get; }

        public SortingMapException()
        {
        }

        public SortingMapException(string message) : base(message)
        {
        }

        public SortingMapException(string message, string entityName, SortablePropertyModel sortModel)
            : base(message)
        {
            SortModel = SortModel;
            EntityName = entityName;
        }

        public SortingMapException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SortingMapException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}