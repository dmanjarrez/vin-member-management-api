﻿using AutoMapper;
using Vin.MemberManagement.Api.Models;
using Vin.MemberManagement.Domain;

namespace Vin.MemberManagement.Api.Automapper
{
    public class MemberProfile : Profile
    {
        public MemberProfile()
        {
            CreateMap<Member, MemberModel>()
                .ForMember(d => d.DateOfBirth, opt => opt.MapFrom(s => s.DateOfBirth))
                .ForMember(d => d.EmailAddress, opt => opt.MapFrom(s => s.EmailAddress.Value))
                .ForMember(d => d.Firstname, opt => opt.MapFrom(s => s.Name.First))
                .ForMember(d => d.Lastname, opt => opt.MapFrom(s => s.Name.Last))
                .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(s => s.PhoneNumber.Value))
                .ForMember(d => d.Username, opt => opt.MapFrom(s => s.Username.Value))
                .ReverseMap();
        }
    }
}